//
//  SaveSafeRoundButton.swift
//  SaveSafe
//
//  Created by Jiaxin Li on 2017/12/29.
//  Copyright © 2017年 FailSafe. All rights reserved.
//

import UIKit

class SaveSafeRoundButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1.0
        layer.borderColor = tintColor.cgColor
        layer.cornerRadius = 8.0
        clipsToBounds = true
        contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        setTitleColor(UIColor.white, for: .normal)
        setTitleColor(UIColor.darkGray, for: .highlighted)
    }
}
