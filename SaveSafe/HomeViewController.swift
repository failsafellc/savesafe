//
//  FirstViewController.swift
//  SaveSafe
//
//  Created by Jiaxin Li on 2017/12/7.
//  Copyright © 2017年 FailSafe. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //titleView configutation
        let titleImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        titleImageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "HomeViewController_TitleIcon")
        titleImageView.image = image
        self.navigationItem.titleView = titleImageView
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

